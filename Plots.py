try:  # for Python 2
    import Tkinter as tk
except ImportError:  # for Python 3
    import tkinter as tk

try:  # for Python 2
    range = xrange
except NameError:
    pass

import numpy as np
import csv
import time
import matplotlib

matplotlib.use('TkAgg', warn=False, force=True)
import matplotlib.pyplot as pyplot
from mpl_toolkits import mplot3d
from matplotlib.backend_tools import ToolBase
from matplotlib.widgets import Slider, Button
from random import randint

matplotlib.rcParams["toolbar"] = "toolmanager"
pyplot.ion()


class Colored2dImmage(tk.LabelFrame):
    def __init__(self, master, zoomLevel, label):
        tk.LabelFrame.__init__(self, master, text=label)
        self.immageWidth = self.master.master.walabot.immageSizeY * zoomLevel
        self.immageHeight = self.master.master.walabot.immageSizeX * zoomLevel
        self.colors = self.createColorRange()

    def createColorRange (self):
        colors=[]
        clorIntensityLevels = 200

        blueBase = "88"
        for i in range(clorIntensityLevels):
            intensity = hex(i).lstrip("0x").rstrip("L").zfill(2)
            colors.append(intensity+intensity+blueBase)

        lightBlueBase = "8888"
        for i in range(clorIntensityLevels):
            intensity = hex(i).lstrip("0x").rstrip("L").zfill(2)
            colors.append(intensity+lightBlueBase)

        greenBase = "88"
        for i in range(clorIntensityLevels):
            intensity = hex(i).lstrip("0x").rstrip("L").zfill(2)
            colors.append(intensity+greenBase+intensity)

        yellowBase = "8888"
        for i in range(clorIntensityLevels):
            intensity = hex(i).lstrip("0x").rstrip("L").zfill(2)
            colors.append(yellowBase+intensity)

        pinkBase = "88"
        for i in range(clorIntensityLevels):
            intensity = hex(i).lstrip("0x").rstrip("L").zfill(2)
            colors.append(pinkBase+intensity+pinkBase)

        redBase = "88"
        for i in range(clorIntensityLevels):
            intensity = hex(i).lstrip("0x").rstrip("L").zfill(2)
            colors.append(redBase+intensity+intensity)

        return colors

    def setGrid(self, sizeX, sizeY):
        height, width = self.immageHeight / sizeX, self.immageWidth / sizeY
        self.cells = [[
            self.canvas.create_rectangle(width * col, height * row, width * (col + 1), height * (row + 1),
                width=0)
            for col in range(sizeY)] for row in range(sizeX)]

    def update(self, rawImage, sizeX, sizeY):
        for i in range(sizeX):
            for j in range(sizeY):
                self.canvas.itemconfigure(self.cells[sizeX - 1 - i][sizeY - 1 - j], fill='#' + self.colors[self.getColorIndex(i, j, rawImage)])

    def getColorIndex(self, i, j, rawImage):
        return rawImage[i][j] if rawImage[i][j] < len(self.colors) else (len(self.colors) - 1)


class Numbered2dImmage(Colored2dImmage):
    def __init__(self, master, zoomLevel, immageNumber):
        Colored2dImmage.__init__(self, master, zoomLevel, 'Raw Image Slice ' + str(immageNumber))
        self.id = immageNumber
        self.canvas = tk.Canvas(self, width=self.immageWidth, height=self.immageHeight)
        self.canvas.bind("<Button-1>", self.clicked)
        self.canvas.pack()
        self.canvas.configure(background='#'+self.colors[0])

    def clicked(self,event):
        self.master.master.setLayerInFocus(self.id)
        self.master.master.focusedLayerChanged = True


class Plot3dPanel():
    def __init__(self, master, label, sizeX, sizeY):
        self.master = master
        self.X, self.Y = np.meshgrid(range(sizeY), range(sizeX))
        axesRange = 55

        self.figure, self.axes, self.plotedData = self.newFigure(label, master.imageLayer, axesRange)
        self.figure.canvas.mpl_connect('button_press_event', self.on_click)

        self.samplesOnX = 21
        self.samplesOnY = 21
        self.layerData = master.imageLayer
        self.layerNumber = 0
        self.selectionFigure, self.selectionFigureAxes, self.selectedArea = self.newFigure("Selection", master.imageLayer, axesRange)
        self.addSliders(axesRange)
        self.addButtons()

        self.coords_points = []
        manager = self.figure.canvas.manager
        savePointTool = manager.toolmanager.add_tool('Save selected point', SavePointTool)
        savePointTool.plot3dPanel = self
        manager.toolbar.add_tool(savePointTool, "toolgroup")

    def newFigure(self, label, image, axesRange):
        figure = pyplot.figure(label)
        self.linkEvents(figure)
        axes = figure.add_subplot(1, 1, 1, projection='3d')
        self.setAxesLabels(axes)
        plotedData = axes.plot_surface(self.X, self.Y, image, rstride=1, cstride=1, cmap='viridis', edgecolor='none')
        self.setAspect(axes, axesRange)
        return figure, axes, plotedData

    def linkEvents(self, figure):
        figure.canvas.mpl_connect('close_event', self.on_close)

    def setAxesLabels(self, axes):
        axes.set_xlabel("Vertical")
        axes.set_ylabel("Horizontal")
        axes.set_zlabel("Signal power")

    def setAspect(sef, axes, upperLimit):
        axes.set_xlim(0, upperLimit)
        axes.set_ylim(0, upperLimit)
        axes.set_aspect("auto")

    def addSliders(self, axesRange):
        verticalSlider = pyplot.axes([0.2, 0.9, 0.5, 0.03])
        horizontalSlider = pyplot.axes([0.2, 0.85, 0.5, 0.03])
        self.verticalPosition = Slider(verticalSlider, 'Vertical Pos', 0, axesRange, valinit=0, valfmt='%d')
        self.horizontalPosition = Slider(horizontalSlider, 'Horizontal Pos', 0, axesRange, valinit=0, valfmt='%d')
        self.verticalPosition.on_changed(self.sliderUpdate)
        self.horizontalPosition.on_changed(self.sliderUpdate)
        self.updateSelectionInProgress = False

    def addButtons(self):
        saveButton = pyplot.axes([0.75, 0.88, 0.18, 0.05])
        self.save = Button(saveButton, 'Save selection')
        self.save.on_clicked(self.saveCallback)

    def saveCallback(self, event):
        startX = int(round(self.horizontalPosition.val))
        startY = int(round(self.verticalPosition.val))
        endX = startX + self.samplesOnX
        endY = startY + self.samplesOnY
        dataToSave = self.layerData[startX:endX, startY:endY]
        fileName = "Slice" + str(self.layerNumber) + \
                   "V" + str(startX) + "to" + str(endX) + \
                   "H" + str(startY) + "to" + str(endY) + \
                   "UID" + str(randint(0, np.amax(dataToSave))) + ".txt"
        file = open(fileName, "w")
        np.savetxt(file, dataToSave, fmt='%d')
        file.close()

    def update(self, rawImage, layerNumber):
        self.layerData = rawImage
        self.layerNumber = layerNumber
        self.plotedData.remove()
        self.plotedData = self.updateFigure(self.figure, self.axes, self.layerData, self.layerNumber)
        self.updateSelection(self.layerData, self.layerNumber, int(round(self.horizontalPosition.val)), int(round(self.verticalPosition.val)))

    def updateFigure(self, figure, axes, image, layerNumber):
        try:
            plotedData = axes.plot_surface(self.X, self.Y, image, rstride=1, cstride=1, cmap='viridis', edgecolor='none')
            axes.set_zlim(0, np.amax(image))
            figure.suptitle("Slice " + str(layerNumber))
            figure.draw()
        except:
            pass
        figure.canvas.flush_events()
        return plotedData

    def sliderUpdate(self, val):
        def callback():
            if (True != self.updateSelectionInProgress):
                self.updateSelection(self.layerData, self.layerNumber, int(round(self.horizontalPosition.val)), int(round(self.verticalPosition.val)))

        self.master.after_idle(callback)

    def updateSelection(self, originalImage, layerNumber, xPosition, yPosition):
        self.updateSelectionInProgress = True
        self.selectedArea.remove()
        selection = self.getSelection(originalImage, xPosition, yPosition)
        self.selectedArea = self.updateFigure(self.selectionFigure, self.selectionFigureAxes, selection, layerNumber)
        self.updateSelectionInProgress = False

    def getSelection(self, originalImage, startX, startY):
        newImage = np.zeros(originalImage.shape)
        newImage[startX:(startX + self.samplesOnX), startY:(startY + self.samplesOnY)] = \
            originalImage[startX:(startX + self.samplesOnX), startY:(startY + self.samplesOnY)]
        return newImage

    def on_close(self, event):
        self.master.onClose()

    def on_click(self, event):
        """Handle click event. If mouse right button was clicked draw a point

            Args:
                self:   Current object.
                event:  Mouse event object.
        """
        if event.button == 3:
            if event.inaxes is not None:
                result = self.get_coords(self.axes.format_coord(event.xdata,event.ydata))
                if len(self.coords_points) != 0:
                    self.coords_points.pop(0).remove()

                self.x, self.y, self.z = result[0], result[1], result[2]
                self.coords_points = self.axes.plot([self.x], [self.y], [self.z],'ro', linewidth=2)

    def get_coords(self, coords):
        """Return array of coords from string.

            Args:
                self:   Current object.
                coords: Coords as string.

            Returns:
                Return coords in array with format [x, y, z].
        """
        result = []
        coords = coords.split(',')
        for coord in coords:
            startIndex = coord.index('=')
            value = coord[startIndex+1:].strip()
            result.append(float(value))
        return result


def closePyplots():
    pyplot.ioff()
    pyplot.close('all')


class SavePointTool(ToolBase):
    description = 'Save selected point coordinates'
    plot3dPanel = None

    def trigger(self, *args, **kwargs):
        if len(self.plot3dPanel.coords_points) == 0:
            self.toolmanager.message_event("Please select point first.")
        else:
            with open('pointCoords_layer' + str(self.plot3dPanel.master.layerInFocus) + '_' + str(time.time()) + '.csv', mode='w') as selected_point_file:
                writer = csv.writer(selected_point_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                writer.writerow([self.plot3dPanel.x, self.plot3dPanel.y, self.plot3dPanel.z, self.plot3dPanel.master.layerInFocus])
                self.toolmanager.message_event("Done! Point added to csv file.")

# MIT License
# 
# Copyright (c) 2018 CircD team
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 
# CircD Team members:
# Florian Petcu - Project Lead
# Viorel Bota - Software developement    
# Gheorghe Catalin Crisan - Software developement    
