from __future__ import print_function, division

# from WalaBotWrapper import Walabot

from WalaBotStub import Walabot

try:  # for Python 2
    import Tkinter as tk
except ImportError:  # for Python 3
    import tkinter as tk

try:  # for Python 2
    range = xrange
except NameError:
    pass

from math import floor

from Plots import Numbered2dImmage, Plot3dPanel, closePyplots

from Menus import MenuWindow, State

import numpy as np

Version = "01.02.02.28"

# TODO fix start stop walabot
class Main(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.master.title("CircD V " + Version)
        self.walabot = Walabot()
        self.last3dRawImmage = np.zeros([self.walabot.immageSizeX, self.walabot.immageSizeY, self.walabot.immageSizeZ], dtype=int)
        self.imageLayer = np.zeros([self.walabot.immageSizeX, self.walabot.immageSizeY], dtype=int)
        self.menuWindow = MenuWindow(self)
        self.master.protocol("WM_DELETE_WINDOW", self.onClose)
        self.menuWindow.grid(rowspan=3, row=0, column=0)
        self.initScannedDataWindow()
        self.initFocusedLayerWindow()
        self.applicationState = State.disconnected
        self.menuWindow.setStatus(self.applicationState)
        self.connectToWalabot()
        self.update_idletasks()
        self.configureWalabot()
        self.update_idletasks()

    def initScannedDataWindow(self):
        scannedLayersWindow = tk.Toplevel(self)
        scannedLayersWindow.title("Scanned layers")
        scannedLayersWindow.protocol("WM_DELETE_WINDOW", self.onClose)
        self.scannedDataLayers = []
        zoom = 2
        layersPerRow = 4
        for z in range(self.walabot.immageSizeZ):
            newLayerCanvas = Numbered2dImmage(scannedLayersWindow, zoom, z)
            newLayerCanvas.setGrid(self.walabot.immageSizeX, self.walabot.immageSizeY)
            newLayerCanvas.grid( row=int(floor(z/layersPerRow)), column=z%layersPerRow )
            self.scannedDataLayers.append(newLayerCanvas)

    def onClose(self):
        if (self.applicationState == State.scanning) or (self.applicationState == State.scanning):
            self.applicationState = State.stopping
            self.menuWindow.setStatus(self.applicationState)
        else:
            self.cleanUp()

    def cleanUp(self):
        closePyplots()
        self.master.quit()

    def initFocusedLayerWindow(self):
        self.setLayerInFocus(0)
        self.focusedLayerWindow = Plot3dPanel(self, "Image in focus", self.walabot.immageSizeX, self.walabot.immageSizeY)

    def connectToWalabot(self):
        try:
            self.walabot.connect()
            if (1 == self.walabot.getStatus()):
                self.applicationState = State.connected
                self.menuWindow.setStatus(self.applicationState)
            else:
                self.menuWindow.setStatus(self.applicationState, 'Could not connect to walabot')
        except:
            self.menuWindow.setStatus(self.applicationState, 'Exception occur while attempting to connect to walabot')

    def configureWalabot(self):
        if self.applicationState != State.connected:
            self.menuWindow.setStatus(self.applicationState, 'Connection to walabot is not established')
            return
        self.walabot.setProfile()
        self.walabot.setArena()
        self.walabot.setFilter()
        if (2 == self.walabot.getStatus()):
            self.applicationState = State.configured
            self.menuWindow.setStatus(self.applicationState)
        else:
            self.menuWindow.setStatus(self.applicationState, 'Could not configure walabot')

    def startDataAquisition(self):
        if (self.applicationState == State.scanning) or (self.applicationState == State.paused):
            self.menuWindow.setStatus(self.applicationState, 'Walabot is already scanning')
            return
        if self.applicationState != State.configured:
            self.menuWindow.setStatus(self.applicationState, 'Connection to walabot is not established')
            return
        self.startWalabot()
        self.update_idletasks()
        self.calibrateWalabot()
        self.update_idletasks()
        if (self.applicationState != State.calibrated):
            self.menuWindow.setStatus(self.applicationState, 'Walabot is not calibrated')
            return
        self.applicationState = State.scanning
        self.scan()

    def startWalabot(self):
        if self.applicationState != State.configured:
            self.menuWindow.setStatus(self.applicationState, 'Connection to walabot is not established')
            return
        self.walabot.start()
        if (3 == self.walabot.getStatus()):
            self.applicationState = State.started
            self.menuWindow.setStatus(self.applicationState)
        else:
            self.menuWindow.setStatus(self.applicationState, "Failed to start walabot")

    def calibrateWalabot(self):
        if self.applicationState != State.started:
            self.menuWindow.setStatus(self.applicationState, 'Walabot is not started')
            return
        self.walabot.calibrate()
        if (3 == self.walabot.getStatus()):
            self.applicationState = State.calibrated
            self.menuWindow.setStatus(self.applicationState)
        else:
            self.menuWindow.setStatus(self.applicationState, "Failed to calibrate walabot")

    def scan(self):
        self.menuWindow.setStatus(self.applicationState)
        self.update_idletasks()
        if State.paused != self.applicationState:
            rawImage, sizeX, sizeY, depth = self.walabot.triggerAndGet3dData()
            self.update_idletasks()
            self.recordImage(rawImage)
            self.update_idletasks()
            self.updateDataWindows(depth, self.last3dRawImmage, sizeX, sizeY)
        if State.paused != self.applicationState or self.focusedLayerChanged:
            self.updateFocusedLayerWindow(self.last3dRawImmage)
        else:
            self.focusedLayerWindow.figure.canvas.flush_events()
        self.update_idletasks()
        if State.stopping == self.applicationState:
            self.cleanUp()
        else:
            self.cyclesId = self.after_idle(self.scan)

    def recordImage(self, rawImage):
            np.copyto(self.last3dRawImmage, rawImage)

    def updateDataWindows(self, depth, rawImage, sizeX, sizeY):
        for z in range(depth):
            np.copyto(self.imageLayer, rawImage[:, :, z])
            self.scannedDataLayers[z].update(self.imageLayer, sizeX, sizeY)
            self.update_idletasks()

    def updateFocusedLayerWindow(self, rawImage):
        np.copyto(self.imageLayer, rawImage[:, :, self.layerInFocus])
        self.focusedLayerWindow.update(self.imageLayer, self.layerInFocus)
        self.focusedLayerChanged = False

    def setLayerInFocus(self,value):
        self.focusedLayerChanged = False
        self.layerInFocus = value

    def loadImageFromFile(self, fileData):
        self.last3dRawImmage = fileData
        self.update_idletasks()
        self.updateDataWindows(self.walabot.immageSizeZ, self.last3dRawImmage, self.walabot.immageSizeX, self.walabot.immageSizeY)
        self.updateFocusedLayerWindow(self.last3dRawImmage)
        self.update_idletasks()


root = tk.Tk()
main = Main(root).pack(side=tk.TOP, fill=tk.BOTH, expand=True)
root.update()
root.mainloop()
try:
    Main.wlbt.gracefullExit()
except:
    pass


# MIT License
# 
# Copyright (c) 2018 CircD team
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 
# CircD Team members:
# Florian Petcu - Project Lead
# Viorel Bota - Software developement
# Gheorghe Catalin Crisan - Software developement    
