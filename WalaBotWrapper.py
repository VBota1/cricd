import platform
from imp import load_source

import numpy as np

if 'Linux'==platform.system():
    walabotPythonLocation = '/usr/share/walabot/python/WalabotAPI.py'
    walabotCLibrary = '/usr/lib/libWalabotAPI.so'
else:
    walabotPythonLocation = 'C:/Program Files/Walabot/WalabotSDK/python/WalabotAPI.py'
    walabotCLibrary = 'C:/Program Files/Walabot/WalabotSDK/bin/x64/WalabotAPI.dll'

WalabotAPI = load_source('WalabotAPI',walabotPythonLocation)
WalabotAPI.Init(walabotCLibrary)

class Walabot:
    def __init__(self):
        self.walabot = WalabotAPI
        self.walabot.Init()
        self.walabot.SetSettingsFolder()
        self.immageSizeX = 41
        self.immageSizeY = 54
        self.immageSizeZ = 21

    def connect(self):
        self.walabot.ConnectAny()

    def setProfile(self):
        self.walabot.SetProfile(self.walabot.PROF_SHORT_RANGE_IMAGING)

    def setArena(self):
        xMinInCM = -6  # -3 # -6
        xMaxInCM = 10  # 3 # 10
        xResolutionInCM = 0.3
        self.walabot.SetArenaX(xMinInCM, xMaxInCM, xResolutionInCM)
        yMinInCM = -6  # -2 # -6
        yMaxInCM = 6  # 5 # 6
        yResolutionInCM = 0.3
        self.walabot.SetArenaY(yMinInCM, yMaxInCM, yResolutionInCM)
        zMinInCM = 2  # 1 # 3
        zMaxInCM = 8  # 6 # 9
        zResolutionInCM = 0.3
        self.walabot.SetArenaZ(zMinInCM, zMaxInCM, zResolutionInCM)
        minSignalPowerToRecord = 40
        self.walabot.SetThreshold(minSignalPowerToRecord);

    def setFilter(self):
        self.walabot.SetDynamicImageFilter(self.walabot.FILTER_TYPE_NONE)

    def start(self):
        self.walabot.Start()

    def calibrate(self):
        self.walabot.StartCalibration()
        while self.walabot.GetStatus()[0] == self.walabot.STATUS_CALIBRATING:
            self.walabot.Trigger()

    def triggerAndGet3dData(self):
        self.walabot.Trigger()
        rawImmage, sizeX, sizeY, depth, power = self.walabot.GetRawImage()
        return np.array(rawImmage), sizeX, sizeY, depth

    def getStatus(self):
        appStatus, calibrationProcess = self.walabot.GetStatus()
        return appStatus

    def stop(self):
        self.walabot.Stop()

    def gracefullExit(self):
        self.stop()
        self.walabot.Disconnect()
