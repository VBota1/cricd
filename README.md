# CricD

Testing Walabot for detecting bones an other internal organs.

The scanned data provided by the Walabot has a resolution of 0.3[cm] on all axes.

The GetRawImage function provides tridimensional (3-D) image data matrix <br />
in which each element represents the reflected power at (x,y,z) spatial location <br />
corresponding to this element indexing in the matrix.

Considering the scanning area configuration we get 21 layers of depth with 54x41 power readings on each layer.

![alt text](debugData/LayersSample.png "walabot scanned layers")<br />
For analysis we represent each of these data layers by mapping power values to a RGB color scale.

Each layer can also be viewed as a 3d plot.<br />
![alt text](debugData/3dLayerRepresentation.png "walabot layer in 3d plot")

We measured the reading speed of the walabot device we currently own at an <br />
average of 848[ms] with a max of 918[ms] and a min of 825[ms] on 36 samples.

However the UI will react slower than that because of the time needed to drow the figures.
<br />
<br />

**The intended operation process is:**<br />
To run execute `python Gui.py`

Put the walabot on the intended surface.<br />
If you want to scan body parts try putting the walabot on the table with the sensors up and add a book on top.

Click the start button.

Wait a couple of seconds for the Walabot to calibrated status to be indicated.<br />

Take the walabot with the book and place it on the desired body part.<br />
Once you are happy with the immage you get click pause.

The window "Scanned Layers" will display the 21 layers of depth returned by the walabot<br />
From the window "Scanned Layers" you can select (by clockin on it) a layer to be detayled <br />
in the window "Image in focus"<br />
The window "Generated Image" represent a in 3d the max power value for each point on all layers

Anytime during the scanning or paused state you can save the data matrix represented by clicking Save.<br />
The data is saved next to the python script in a file named immageDataX.txt, where X starts from 0 ar each mesurement start.<br />
Be carefull not to overwrite old saved data when restarting the script

During the paused state you can also load previously saved data files by clicking the load button.
<br />
<br />

**I you don't have a walabot hardware** you can use the sciprts to checkout the data saved here:<br />
https://gitlab.com/VBota1/cricd/tree/master/debugData<br />
To do this you must first replace the WalaBotWrapper library with the WalaBotStub.<br />
You can achive this by replacing in the Gui.py script the line:<br />
`from WalaBotWrapper import Walabot`<br />
with the line:<br />
`from WalaBotStub import Walabot`