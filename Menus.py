try:  # for Python 2
    import Tkinter as tk
except ImportError:  # for Python 3
    import tkinter as tk

try:  # for Python 2
    range = xrange
except NameError:
    pass

try:  # for Python 2
    import tkFileDialog
except:
    from tkinter import filedialog as tkFileDialog

from tkinter import ttk, Label, Entry, StringVar
from random import randint

from enum import Enum

import numpy as np

import time

class State(Enum):
    disconnected='Disconnected'
    connected = 'Connected'
    configured = 'Configured'
    started = 'Started'
    calibrated = 'Calibrated'
    scanning = 'Scanning'
    paused = 'Paused'
    stopping = 'Stopping'
    undefined = 'Undefined'


class StatusWindow(tk.LabelFrame):
    def __init__(self, master):
        self.messageBoard = tk.Frame(master)
        self.messageBoardNoOfRows = 25
        self.initTextTable()
        self.freeLineOnMessageBoard = 0

    def initTextTable(self):
        self.labels = []
        self.labelValues = []
        for i in range(0, self.messageBoardNoOfRows):
            self.labelValues.append(StringVar(value=''))
            self.labels.append(tk.Label(self.messageBoard, textvariable=self.labelValues[i], anchor='w'))
            self.labels[i].grid(row=i, column=0, sticky="W")

    def writeToMessageBoard(self, text):
        if self.freeLineOnMessageBoard+1>self.messageBoardNoOfRows:
            self.resetMessageBoard()
        self.labelValues[self.freeLineOnMessageBoard].set(text)
        self.freeLineOnMessageBoard = self.freeLineOnMessageBoard + 1

    def resetMessageBoard(self):
        self.freeLineOnMessageBoard = 0
        for i in range(self.freeLineOnMessageBoard, self.messageBoardNoOfRows):
            self.labelValues[i].set('')


class MenuWindow(tk.LabelFrame):
    def __init__(self, master):
        tk.LabelFrame.__init__(self, master, text='Menu')
        self.buttonsFrame = tk.Frame(self)
        self.savedFileIndex = 0
        self.runButton, self.pause, self.save = self.createMenuEntryes(self.buttonsFrame)
        self.statusWindow = StatusWindow(self)
        self.lastPrintedState = State.undefined
        self.buttonsFrame.grid(row=0, column=0, sticky=tk.W)
        self.statusWindow.messageBoard.grid(row=self.statusWindow.messageBoardNoOfRows, columnspan=3, sticky=tk.W)

    def createMenuEntryes(self, frame):
        loadImmageButton, pauseButton, runButton, saveImmageButton = self.createButtons(frame)
        self.createFileNameInputFields(frame)
        self.arrangeMenuElements(frame, loadImmageButton, pauseButton, runButton, saveImmageButton)
        return runButton, pauseButton, saveImmageButton

    def createButtons(self, frame):
        runButton = tk.Button(frame, text='Start', command=self.start, width=6)
        pauseButton = tk.Button(frame, text='Pause', command=self.pause, width=6)
        loadImmageButton = tk.Button(frame, text='Load', command=self.load, width=6)
        saveImmageButton = tk.Button(frame, text='Save', command=self.save, width=6)
        return loadImmageButton, pauseButton, runButton, saveImmageButton

    def createFileNameInputFields(self, frame):
        self.sex = ttk.Combobox(frame, values=["M", "F"], width=2)
        self.sex.set("M")
        self.heightLabel = "H"
        self.height = Entry(frame, width=4);
        self.weightLabel = "W"
        self.weight = Entry(frame, width=4);
        self.ageLabel = "A"
        self.age = Entry(frame, width=4);
        self.uidLabel = "UID"
        self.uid = str(randint(0, 64000)).ljust(5)

    def arrangeMenuElements(self, frame, loadImmageButton, pauseButton, runButton, saveImmageButton):
        runButton.grid(row=0, column=0)
        pauseButton.grid(row=1, column=0)
        loadImmageButton.grid(row=2, column=0)
        saveRow = 3
        saveImmageButton.grid(row=saveRow, column=0)
        self.sex.grid(row=saveRow, column=1)
        Label(frame, text=self.heightLabel, width=2).grid(row=saveRow, column=2)
        self.height.grid(row=saveRow, column=3)
        Label(frame, text=self.weightLabel, width=2).grid(row=saveRow, column=4)
        self.weight.grid(row=saveRow, column=5)
        Label(frame, text=self.ageLabel, width=2).grid(row=saveRow, column=6)
        self.age.grid(row=saveRow, column=7)
        Label(frame, text=self.uidLabel, width=3).grid(row=saveRow, column=8)
        Label(frame, text=self.uid, width=5).grid(row=saveRow, column=9)
        ttk.Separator(frame, orient="horizontal").grid(row=saveRow + 1, columnspan=7)

    def start(self):
        self.master.startDataAquisition()

    def pause(self):
        if (State.paused != self.master.applicationState) and (State.scanning != self.master.applicationState):
            self.setStatus(self.master.applicationState,"Pause can only be toggeled during scanning operation")
            return
        if State.paused == self.master.applicationState:
            self.master.applicationState = State.scanning
        else:
            self.master.applicationState = State.paused

    def save(self):
        if (State.paused != self.master.applicationState) and (State.scanning != self.master.applicationState):
            self.setStatus(self.master.applicationState,"Save can only be toggeled during scanning operation")
            return
        fileName = self.sex.get() + \
                   self.heightLabel + self.height.get() + \
                   self.weightLabel + self.weight.get() + \
                   self.ageLabel + self.age.get() + \
                   self.uidLabel + self.uid + ".txt"
        file = open(fileName, "w")
        for slice in range(0, self.master.walabot.immageSizeZ):
            file.write('# New layer\n')
            np.savetxt(file, self.master.last3dRawImmage[:, :, slice], fmt='%d')
        file.close()
        self.setStatus(fileName + " saved")
        self.savedFileIndex=self.savedFileIndex+1

    def load(self):
        if State.paused != self.master.applicationState:
            self.setStatus(self.master.applicationState, "Image can only be loaded in paused state")
            return
        self.fileName = ""
        self.fileContent = np.zeros((self.master.walabot.immageSizeZ, self.master.walabot.immageSizeX, self.master.walabot.immageSizeY))

        def callback():
            self.fileName = tkFileDialog.askopenfilename()
            if self.fileName:
                self.fileContent = np.loadtxt(self.fileName, dtype=int).reshape(self.master.walabot.immageSizeZ, self.master.walabot.immageSizeX, self.master.walabot.immageSizeY)
            self.master.master.update()
            if self.fileName:
                reproducedData = np.zeros((self.master.walabot.immageSizeX, self.master.walabot.immageSizeY, self.master.walabot.immageSizeZ), dtype=int)
                for z in range(0, self.master.walabot.immageSizeZ):
                    for y in range(0, self.master.walabot.immageSizeY):
                        reproducedData[:, y, z] = self.fileContent[z, :, y].transpose()
                self.master.loadImageFromFile(reproducedData)
                self.setStatus("Loaded: " + self.fileName)
        self.after_idle(callback)

    def setStatus (self, newState, text = ''):
        if ''!=text:
            self.statusWindow.writeToMessageBoard(text)
            self.statusWindow.writeToMessageBoard(newState)
        elif self.lastPrintedState!=newState:
            self.lastPrintedState=newState
            self.statusWindow.writeToMessageBoard(newState)
            
            

# MIT License
# 
# Copyright (c) 2018 CircD team
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 
# CircD Team members:
# Florian Petcu - Project Lead
# Viorel Bota - Software developement        
# Gheorghe Catalin Crisan - Software developement         
